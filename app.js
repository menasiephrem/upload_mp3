const Walk = require("@root/walk");
const cloudinary = require('cloudinary').v2;
const path = require("path");

cloudinary.config({ 
    cloud_name: 'evolunt', 
    api_key: '513741497493487', 
    api_secret: 'kwk0cBdQXiGA5fFaXbj8VIlLAoc' 
});

const filePath = process.argv.pop()

// walkFunc must be async, or return a Promise
const walkFunc = async (err, pathname, dirent) => {
    if (err) {
        // throw an error to stop walking
        // (or return to ignore and keep going)
        console.warn("fs stat error for %s: %s", pathname, err.message);
        return Promise.resolve();
    }


    if (dirent.isDirectory() && dirent.name.startsWith(".")) {
        return Promise.resolve(false);
    }

    if(dirent.isFile() && dirent.name.split(".").pop() === "mp3"){
            // fs.Dirent is a slimmed-down, faster version of fs.Stats
            
        console.log("started Uploading => ", pathname.replace(filePath, ""));
        const res = await uploadMp3(pathname, pathname.replace(filePath, ""))
        console.log(res.secure_url)
        return Promise.resolve()
    }
}

const uploadMp3 =  (path, url) => {
    return new Promise((resolve, reject) => {
        const opt = {
            resource_type: "raw",
            public_id: "apostolicSongsMp3/" + url,
            overwrite: true
        };

        cloudinary.uploader.upload(path, opt, (error, res) => {
            if(error){
                reject(error)
            }
            resolve(res)
        });
    })

}

Walk.walk(filePath, walkFunc).then(function () {
    console.log("Done");
  });